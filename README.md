# collapse-aninhado

Collapse aninhado
<!--
<template>
  <section class="perguntas-frequentes">
    <div class="row">
      <div class="column">
        <div class="container">
          <h2 class="title">
            Perguntas frequentes
          </h2>
          <p @click="toggleListaQuestion"  class="controler-toggle-mobile">
            <img 
              class="toggle-all" 
              src="@/assets/img/detalhe/layouts-personalizados/desincha-misto/svg/icone-arrow.png"
            >
            CLIQUE PARA ABRIR
          </p>
          <ul ref="listaQuestion" class="perguntas">
            <li 
              v-for="(pergunta, index) in data.perguntas"
              :key="index"
              class="wrapper-pergunta"
            >
              <div @click="toggleQuestion" class="pergunta">
                <h3 class="pergunta-title">
                  {{pergunta.title}}
                </h3>
                <img class="toggle-pergunta" src="@/assets/img/detalhe/layouts-personalizados/desincha-misto/svg/icone-arrow.png">
              </div>
              <div class="resposta">
                <p 
                  v-html="pergunta.resposta"
                  class="resposta-text"
                >
                </p>
                <img @click="toggleQuestion" 
                  class="toggle-resposta" 
                  src="@/assets/img/detalhe/layouts-personalizados/desincha-misto/svg/icone-arrow.png"
                >
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
</template>

<script>
export default {
  name: "perguntasFrequentes",
  props: {
    data: {
      type: Object,
      required: true
    }
  },
  data() {
    return {
      timer: false,
      timerHeight: false,
    }
  },
  methods: {
    toggleListaQuestion(event) {
      const controller = event.currentTarget;
      const toggleAll = controller.children[0];
      const listaQuestion = controller.parentElement.children[2];

      if (listaQuestion.offsetHeight) {
        listaQuestion.style.height = `0`;
        listaQuestion.style.opacity = `0`;
        toggleAll.style.transform = `rotate(-90deg)`;
        
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
          listaQuestion.style.display = `none`;
        }, 300);
      } else {
        this.handleHeights(listaQuestion, true)
          .then(height => {
            listaQuestion.style.height = `${height}px`;
            listaQuestion.style.opacity = `1`;
            toggleAll.style.transform = `rotate(90deg)`;
          });
      }
    },
    toggleQuestion(event) {
      let perguntaDOM;

      if (event.currentTarget.tagName === "DIV") {
        perguntaDOM = event.currentTarget;
      } else {
        perguntaDOM = event.currentTarget.parentElement.parentElement.children[0];
      }

      const respostaDOM = perguntaDOM.parentElement.children[1];
      const togglePergunta = perguntaDOM.children[1]
      const toggleResposta = respostaDOM.children[1];

      if (respostaDOM.offsetHeight) {
        this.animationClosed(respostaDOM, togglePergunta, toggleResposta);
      } else {
        this.animationOpen(respostaDOM, togglePergunta, toggleResposta);
      }
    },
    handleHeights(elementDOM, elementNone, listDOM, closed){
      return new Promise(resolve => {
        let heightRespostaDOM;
        if (elementNone) {
          elementDOM.style.display = "block"
          elementDOM.style.height = "unset";
          heightRespostaDOM = elementDOM.offsetHeight;
          elementDOM.style.height = "0px";
        } else if (closed){
          heightRespostaDOM = elementDOM.offsetHeight;
        } else {
          elementDOM.style.height = "unset";
          heightRespostaDOM = elementDOM.offsetHeight;
          elementDOM.style.height = "0px";
        }
        if (listDOM) {
          if (closed) {
            const heightListDOM = this.$refs.listaQuestion.offsetHeight;
            this.$refs.listaQuestion.style.height = `${heightListDOM - heightRespostaDOM}px`; 
          } else {
            const heightListDOM = this.$refs.listaQuestion.offsetHeight;
            this.$refs.listaQuestion.style.height = `${heightListDOM + heightRespostaDOM}px`; 
          }
        } else {
          this.$refs.listaQuestion.style.height = "unset"
        }
        
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
          resolve(heightRespostaDOM);
        }, 50);
      });
    },
    animationOpen(respostaDOM, togglePergunta, toggleResposta) {
      this.handleHeights(respostaDOM, false, window.innerWidth <= 1080, false)
        .then(height => {
          respostaDOM.style.height = `${height}px`;
          respostaDOM.style.opacity = `1`;

          togglePergunta.style.opacity = `0`;
          togglePergunta.style.transform = `rotate(-270deg)`;
          toggleResposta.style.transform = `rotate(-270deg)`;
        });
    },
    animationClosed(respostaDOM, togglePergunta, toggleResposta) {
      this.handleHeights(respostaDOM, false, window.innerWidth <= 1080, true)
        .then(() => {
          respostaDOM.style.height = `0px`;
          respostaDOM.style.opacity = `0`;
          togglePergunta.style.opacity = `1`;
          togglePergunta.style.transform = `rotate(270deg)`;
          toggleResposta.style.transform = `rotate(270deg)`;
        });
    },
  },
}
</script>
-->
